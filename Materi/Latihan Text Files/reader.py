while True:
    nama_file = input("Masukkan nama file yang ingin dibaca:")
    print("Anda ingin mencari text bernama", nama_file)
    print()
    try:
        file_object = open(nama_file,"r")
        for row in file_object:
            print(row, end="")            
        print()
    except FileNotFoundError:
        print("File teks bernama", nama_file,"tidak ditemukan")
        continue #setelah continue berjalan maka akan langsung kembali ke looping while
    file_object.close()
    print("Program Selesai!")
    break    