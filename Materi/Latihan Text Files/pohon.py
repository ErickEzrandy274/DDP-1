#sudut masih berupa derajat
sudut = int(input("Sudut dari pengamat ke pucuk pohon: "))
jarak_pengamat = int(input("Jarak dari pengamat ke pucuk pohon: "))

import math
tinggi = jarak_pengamat * math.tan(math.radians(sudut))
tinggi_pohon = round(tinggi,2)
print("Tinggi pohon tersebut adalah", tinggi_pohon,"m")