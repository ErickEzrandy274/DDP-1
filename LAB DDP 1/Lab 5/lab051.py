# List daftar_keranjang untuk menyimpan semua keranjang
daftar_keranjang = []

def beli_keranjang(nama_keranjang, kapasitas_keranjang):
     daftar_keranjang.append([nama_keranjang,kapasitas_keranjang])#menambahkan ke daftar keranjang
     print("Berhasil menambahkan",nama_keranjang,"dengan kapasitas",kapasitas_keranjang)

def jual_keranjang(indeks):
     A = daftar_keranjang.pop(indeks)
     print("Berhasil menjual",A[0],"yang memiliki kapasitas sebesar",A[1])

def ubah_kapasitas(indeks, kapasitas_baru):
     kapasitas_lama = daftar_keranjang[indeks][0]
     daftar_keranjang[indeks][1] = kapasitas_baru
     kapasitas_baru1 = daftar_keranjang[indeks][1]
     print("Berhasil mengubah kapasitas",kapasitas_lama,"menjadi",kapasitas_baru1)

def ubah_nama(indeks, nama_baru):
     nama_lama = daftar_keranjang[indeks][0]
     daftar_keranjang[indeks][0] = nama_baru
     print("Berhasil mengubah nama",nama_lama,"menjadi",nama_baru)

def lihat(indeks):
     print("Keranjang",daftar_keranjang[indeks][0],"memiliki kapasitas sebesar",daftar_keranjang[indeks][1])

def lihat_semua():
    print("Keranjang Dek Depe")
    print("-"*30)
    for i in range(len(daftar_keranjang)):
        print("{:10s} | {:6d}".format(daftar_keranjang[i][0],int(daftar_keranjang[i][1])))

def total_kapasitas():
    total = 0 #menampung setiap penjumlahan kapasitas
    for i in range(len(daftar_keranjang)):
        total += int(daftar_keranjang[i][1])
    jumlah_kapasitas = f"Total kapasitas keranjang Dek Depe adalah {total}"
    return jumlah_kapasitas

jumlah_operasi = int(input("Masukkan banyak operasi: "))
for i in range(jumlah_operasi):
     operasi = input("Masukkan operasi: ")
     tampungan = operasi.split()
     if tampungan[0] == "BELI":
          beli_keranjang(tampungan[1],tampungan[2])
     elif tampungan[0] == "JUAL": 
          jual_keranjang(int(tampungan[1])) 
     elif tampungan[0] == "UBAH_KAPASITAS":
          ubah_kapasitas(int(tampungan[1]),tampungan[2])
     elif tampungan[0] == "UBAH_NAMA":
          ubah_nama(int(tampungan[1]),tampungan[2])   
     elif tampungan[0] == "LIHAT":
          lihat(int(tampungan[1]))
     elif tampungan[0] == "LIHAT_SEMUA":
          lihat_semua()
     else:
          print(total_kapasitas())   
     print()