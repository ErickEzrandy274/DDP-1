#kita diminta untuk masukin nama tactical doll yang ingin dibeli
print("### REQUEST TACTICAL DOLL ###")
nama= input("Masukkan nama Tactical Doll: ")

#kita diminta untuk memasukkan nilai atributnya
firepower1=int(input("Masukkan Firepower: "))
rate_of_fire1=int(input("Masukkan Rate of Fire: "))
accuracy1=int(input("Masukkan Accuracy: "))
evasion1=int(input("Masukkan Evasion: "))

'''
Setelah masukkin nilai atributnya, kita akan menghitung damage per second
dan combat effectiveness dari robot yang kita inginkan
'''
damage_per_second1=(firepower * rate_of_fire)/60
damage_per_second=round(damage_per_second1,2)
combat_effectiveness1= (30 * firepower) + (40 * rate_of_fire **2/120)+ 15 *(accuracy + evasion)
combat_effectiveness=round(combat_effectiveness1)

#pake fungsi dibawah ini biar ada jeda antara minta masukkan dengan hasil yang akan diperoleh
print(" ")

#sekarang kita print hasil dari masukkan tadi
print("### SUCCESS ###")
print("Tactical Doll: ", nama)
print("Firepower: ", firepower)
print("Rate of Fire: ", rate_of_fire)
print("Accuracy: ", accuracy)
print("Evasion: ", evasion)
print("Damage per Second: ", damage_per_second)
print("Combat Effectiveness: ", combat_effectiveness)

