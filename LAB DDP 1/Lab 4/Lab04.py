in_filename = input("Masukkan nama file input: ")

try:
	with open(in_filename, "r+") as myfile:
		isi = myfile.readlines()
		if len(isi) > 0:
			min_len = 10 ** 9
			max_len = 0
			for baris in isi:
				min_len = min(len(baris), min_len)
				max_len = max(len(baris), max_len)
				
			print("#" * 11, file = myfile)
			print(f"Min: {min_len} karakter", file = myfile)
			print(f"Max: {max_len} karakter", file = myfile)
			myfile.write(f"Range: {max_len - min_len} karakter\n")		
		else:
			print("NULL", file = myfile)
			
except FileNotFoundError:
	print("File tidak ditemukan :(")

else: # Diisi statement yang dijalankan ketika program berjalan tanpa error
	print(f"Output berhasil ditulis pada {in_filename}")

input("Program selesai. Tekan enter untuk keluar...")
