file_input = input("Masukkan nama file input: ")
file_output = input("Masukkan nama file output: ")

try:
    assert file_input != file_output #nama file input dan output tidak boleh sama
    jumlah_karakter = 0
    jumlah_baris = 0

    with open(file_input,"r") as in_file:
        for baris in in_file:
            jumlah_baris += 1 #hitung jumlah baris
            jumlah_karakter += len(baris) - 1 #-1 karena ada new line di setiap akhir baris
    
    with open(file_output, "w") as in_file:
        print("Total jumlah baris adalah {} dan jumlah karakternya adalah {}".format(jumlah_baris,jumlah_karakter),file=in_file)
        print("Rata-rata baris mengandung {} karakter".format(jumlah_karakter/jumlah_baris), file=in_file)

except FileNotFoundError:
    print("File tidak ditemukan :(")

except AssertionError:
    print("Nama file input harus berbeda dengan nama file output")

else:
    print("Output berhasil ditulis pada {}".format(file_output))

input("Program selesai. Tekan enter untuk keluar...")
    
