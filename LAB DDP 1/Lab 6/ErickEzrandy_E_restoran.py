jumlah_donat = int(input("Masukkan Jumlah Donat DUAARRR!: "))
dictionary1 = {} #keynya nama donat
dictionary2 = {} #keynya harga
dictionary3 = {} #harga maksimal
dictionary4 = {}
donat_terjual = set()

for i in range(1,jumlah_donat+1):
    spesifikasi = input(f"Data {i}: ").split()
    dictionary1[spesifikasi[0]] = spesifikasi[1] #key berupa nama donat
    dictionary2[spesifikasi[1]] = spesifikasi[2] #key berupa harga
    dictionary3[spesifikasi[2]] = spesifikasi[1] #key berupa nama rasa
    dictionary4[spesifikasi[1]] = spesifikasi[0] 
print()  
jumlah_pembeli = int(input("Masukkan Jumlah Pembeli: "))
tot_pendapatan = 0    

for k in range(1,jumlah_pembeli+1):
    pembeli = input(f"Pembeli {k}: ").split() #buat pisahain jenis pembelian 
    if pembeli[0] == "BELI_NAMA":
        if pembeli[1] in dictionary1:
            print(f"{pembeli[1]} terjual dengan harga {dictionary1[pembeli[1]]}")
            donat_terjual.add(pembeli[1]) #dimasukkan ke set barang yang terjual
            tot_pendapatan += int(dictionary1[pembeli[1]]) #menambahkan pendapatan
            del dictionary1[pembeli[1]]
        else:
            print(f"Tidak ada Donat DUAARRR!!! dengan nama {pembeli[1]} :(")
    else:
        if pembeli[1] in dictionary3:
            print(f"{dictionary4[dictionary3[pembeli[1]]]} terjual dengan harga {dictionary3[pembeli[1]]}")
            donat_terjual.add(dictionary4[dictionary3[pembeli[1]]])
            tot_pendapatan += int(dictionary3[pembeli[1]])
        else:
            print(f"Tidak ada Donat DUAARRR!!! dengan rasa {pembeli[1]} :(")
print()            
print("Donat Terjual: ",end="")            
for elem in donat_terjual: #cetak elemen di set donat_terjual   
    print(elem,end=" ")
print(f"\nTotal Pendapatan: {tot_pendapatan}") #cetak hasil uang yang didapat