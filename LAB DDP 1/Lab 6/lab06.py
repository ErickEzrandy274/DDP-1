N = int(input("Masukkan Jumlah Donat DUAARRR!!!: "))
donat_dict = dict()
for i in range(N):
    donat_input = input(f"Data {i+1}: ").split()
    donat_dict[donat_input[0]] = {"Harga": int(donat_input[1]), "Rasa": donat_input[2]}
 
sold = set()
profit = 0
 
Buyer = int(input("Masukkan Jumlah Pembeli: "))
for i in range(Buyer):
    found = False
    buyer_input = input(f"Pembeli {i+1}: ").split()
 
    if buyer_input[0] == "BELI_NAMA":
        nama_donat = buyer_input[1]
        if nama_donat not in donat_dict:
            print(f"Tidak ada Donat DUAARRR!!! dengan nama {nama_donat}")
        else:
            found = True
            harga_donat = donat_dict[nama_donat]['Harga']
 
    elif buyer_input[0] == "BELI_RASA":
        rasa_donat = buyer_input[1]
        max_harga = 0
        for donat in donat_dict:
            if donat_dict[donat]['Rasa'] == rasa_donat:
                found = True
                if donat_dict[donat]['Harga'] > max_harga:
                    nama_donat = donat
                    harga_donat = donat_dict[donat]['Harga']
                    max_harga = harga_donat
        if not found:    
            print(f"Tidak ada Donat DUAARRR!!! dengan rasa {rasa_donat}")
 
    if found:
        print(f"{nama_donat} terjual dengan harga {harga_donat}")
        profit += harga_donat
        sold.add(nama_donat)
 
print("Donat Terjual: ", end="")
print(*sold, sep=", ")
 
print(f"Total Pendapatan: {profit}")
