from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename

class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()

    def initUI(self):
        self.master.title("Pacil Editor") #bikin judul window
        self.master.geometry("668x466")
        frame1 = Frame(self.master)
        frame1.pack(anchor='e', side='top')

    def create_buttons(self):
        button1 = Button(self.master, text="Open File", command=self.load_file, fg='red',activebackground='yellow') #tombol Open File  
        button2 = Button(self.master, text="Save File", command=self.save_file, fg='blue',activebackground='orange') 
        button3 = Button(self.master, text="Quit Program", command=quit, fg='red', activeforeground='purple',activebackground='green')
        button1.pack(padx=4,pady=4)
        button2.pack(padx=4,pady=4)
        button3.pack(padx=4,pady=4)

    def create_editor(self):
        self.text = Text(self.master, width=100) #membuat text box agar user bisa menulis sesuatu
        self.text.pack()
       
    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()
        self.text.delete("1.0", END) #hapus semua tulisan di text box yang gamau di save
        self.text.insert("1.0", result) #masukkin tulisan
    
    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        file_name1 = open(file_name, "w") #buka nama file yang dituju buat nyimpan tulisan yang ditulis di text box
        file_name1.write(self.text.get("1.0", END))
        file_name1.close()

if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()