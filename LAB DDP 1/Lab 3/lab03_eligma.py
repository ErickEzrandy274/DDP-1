#DIJAMIN minimal mengandung satu angka dan tidak ada huruf kapital
inputan_string=input("Masukkan string: ")
kumulatif = 0
string1=""
string2=""
#ngecek apakah ada angka dan huruf
for i in inputan_string:
    if i.isalpha():
        string1+=i
    elif i.isnumeric():
        kumulatif+=int(i)
#buat melakukan pergeseran hurufnya
for i in string1:
    step= ord(i)+kumulatif
    if step > 122:
        step=(step%122)+96 #kalo stepnya udh lebih dari 122 maka harus balik lagi ke a dengan nilai ordnya 96, dengan cara pake modulo
    string2+=chr(step)
print(string2)