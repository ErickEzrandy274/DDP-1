print("Posisi awal Meong Bross berada di kooordinat (0,0)")
jumlah_perintah = int(input("Banyak perintah: "))
x = 0
y = 0
if jumlah_perintah < 0:
    jumlah_perintah = int(input("Banyak perintah: "))
journey = "(0,0)"
#journey = [(0,0)]  (Buat pake list)
for i in range(jumlah_perintah):
    perintah = input("Masukkan perintah: ")
    if perintah == "U":
        y+=1
    elif perintah == "S":
        y-=1
    elif perintah == "T":
        x+=1
    elif perintah == "B":
        x-=1
    elif perintah == "HOME":
        break
    journey += "-("+ str(x) + "," + str(y)+")"
    #pake cara di atas supaya tiap ngelooping disave (cara string)
    #journey.append((x, y)) (buat list)
print("Jalur yang ditemuh meong bross adalah ",journey)
''' 
langsung aja di-append karena list itu sifatny mutable jadi gaperlu assign lagi
x dan y disimpan dalam bentuk tuple karena list bisa nampung semua tipe data
'''
'''
for j in range(len(journey)):
    print(journey[j], end="-")  
print("adalah jalur yang ditempuh oleh meong bross ")
'''
