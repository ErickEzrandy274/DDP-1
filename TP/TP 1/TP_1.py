#Berkolaborasi dengan Indra
#looping menu supaya kalau user masukkan operasi 1-6 tetap mengulang pilihan menunya
while True:
    print("Selamat datang di Program Konverter Bilangan")
    print("     1. Decimal ke Ternary")
    print("     2. Ternary ke Decimal")
    print("     3. Decimal ke Septenary")
    print("     4. Septenary ke Decimal")
    print("     5. Ternary ke Septenary")
    print("     6. Septenary ke Ternary")
    print("     7. Keluar")
    operasi1 = input("Masukkan operasi yang ingin dilakukan: ")
    #handling eror menu dengan cara try and except
    try: #jika user masukkin inputan menu antara 1 dan 7 maka akan masuk ke try dan akan dicocokkan sesuai selection yang ada
        operasi = int(operasi1)        
        if operasi == 1:
            try:
                angka = int(input("Masukkan input: angka: "))
                if angka < 0:
                    raise AssertionError
                number = angka 
                A = []
                while angka != 0: #selama angka yang dimasukkan user bukan 0 dan hasil bagi belum 0 maka akan dibagi terus sampai hasil bagi bernilai 0
                    B = angka % 3 
                    A.append(str(B)) #menyimpan tiap sisa pembagian lalu ditampung di list
                    angka //= 3 #decimal ke ternary itu dibagi 3 terus
                C = "".join(A) #isi dari list tersebut diubah ke string 
                hasil = C[::-1] #membaca tampungan list dari belakang karena kalau ternary itu yang dibaca sisa dari yang paling terkakhir didapat
                print(f"Nilai ternary dari decimal {number} adalah {hasil}") 

            except ValueError:
                print("Input harus harus berupa angka desimal")

            except AssertionError:
                print("Input harus lebih besar dari 0!")    

        elif operasi == 2:
            try:   
                angka = input("Masukkan input: angka: ")   
                number = int(angka)
                if number < 0 or number > 2:
                    raise AssertionError
                n = 1 #buat pengurangan pangkat
                jumlah = 0 #buat nampung hasil perkalian dengan basis 3
                for i in range(len(angka)):
                    number1 = int(angka[i])*(3**(len(angka)-n)) #dikalikan 3 karena ini dari basis 3 dan saya mulai buat decimal dengan mengalikan angka yang paling depan
                    n += 1 #bertambah 1 karena saya mulai hitungnya dari depan maka pangkatnya akan selalu berkurang
                    jumlah += number1 #hasil perkaliannya dimasukkan ke variable jumlah
                print(f"Nilai decimal dari ternary {number} adalah {jumlah}")

            except ValueError:
                print("Input harus harus berupa angka ternary")

            except AssertionError:
                print("Input harus lebih besar dari 0!")       

        elif operasi == 3:
            try:
                angka = int(input("Masukkan input: angka: "))
                if angka < 0:
                    raise AssertionError 
                number = angka
                A = []
                while angka != 0:
                    B = angka % 7
                    A.append(str(B)) 
                    angka //= 7 #decimal ke septenary dibagi 7 terus hingga hasil bagi 0 barulah menjadi septenary dengan melihat sisa baginya
                C = "".join(A)
                hasil = C[::-1]
                print(f"Nilai septenary dari decimal {number} adalah {hasil}") 

            except ValueError:
                print("Input harus harus berupa angka decimal")

            except AssertionError:
                print("Input harus lebih besar dari 0!")       

        elif operasi == 4:
            try:
                angka = input("Masukkan input: angka: ")
                number = int(angka)
                if number < 0 or number > 6:
                    raise AssertionError 
                n = 1
                jumlah = 0
                for i in range(len(angka)):
                    number1 = int(angka[i])*(7**(len(angka)-n)) #dikalikan 7 karena ini dari basis 7 dan saya mulai buat decimal dengan mengalikan angka yang paling depan
                    n += 1 
                    jumlah += number1 
                print(f"Nilai decimal dari septenary {number} adalah {jumlah}")

            except ValueError:
                print("Input harus harus berupa angka septenary")

            except AssertionError:
                print("Input harus lebih besar dari 0!")      

        elif operasi == 5:
            try:
                angka = input("Masukkan input: angka: ")
                #ubah dulu dari ternary ke decimal baru dari decimal ke septenary
                number = int(angka)
                if number < 0 or number > 2:
                    raise AssertionError 
                n = 1
                jumlah = 0
                for i in range(len(angka)):
                    number1 = int(angka[i])*(3**(len(angka)-n))
                    n += 1
                    jumlah += number1
                X = jumlah #ini hasil decimal dari ternary
                A = []
                while X != 0: #looping untuk diubah ke septenary
                    B = X % 7
                    A.append(str(B)) 
                    X //= 7
                C ="".join(A)
                hasil = C[::-1]
                print(f"Nilai septenary dari ternary {number} adalah {hasil}")

            except ValueError:
                print("Input harus harus berupa angka ternary")

            except AssertionError:
                print("Input harus lebih besar dari 0!")    

        elif operasi == 6:
            try:
                angka = input("Masukkan input: angka: ")
                #ubah dulu dari septenary ke decimal baru dari decimal ke ternary
                number = int(angka)
                if number < 0 or number > 6:
                    raise AssertionError 
                n = 1
                jumlah = 0
                for i in range(len(angka)):
                    number1 = int(angka[i])*(7**(len(angka)-n))
                    n += 1
                    jumlah += number1
                X = jumlah #hasil decimal dari septenary
                A = []
                while X != 0:
                    B = X % 3
                    A.append(str(B))
                    X //= 3
                C = "".join(A)
                hasil = C[::-1]
                print(f"Nilai ternary dari decimal {angka} adalah {hasil}") 

            except ValueError:
                print("Input harus harus berupa angka septenary")

            except AssertionError:
                print("Input harus lebih besar dari 0!")

        elif operasi == 7: #ketika user memasukkan 7 pada inputan menu maka program akan berhenti
            print("Terima kasih telah menggunakan program")
            break

        else: #jika user memasukkan inputan lebih dari 7 maka akan ke print seperti di bawah ini
            print("Maaf input tidak valid!")

    except ValueError: #jika user memasukkan inputan menu tidak berada di antara 1 dan 7 maka akan ditangkap oleh except 
        print("Maaf input Anda salah!")