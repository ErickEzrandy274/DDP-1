#Berkolaborasi dengan Indra
import os, random
def start_game():
    print("""
     _                                      
    | |__   ___ _ __ _ __   __ _  ___ _   _ 
    | '_ \\ / _ \\ '__| '_ \\ / _` |/ __| | | |
    | |_) |  __/ |  | |_) | (_| | (__| |_| |
    |_.__/ \\___|_|  | .__/ \\__,_|\\___|\\__,_|
                    |_|                DALAM
        ██╗      ██╗ ██████╗  ██╗ ██╗  ██╗
        ██║      ██║ ██╔══██╗ ██║ ██║ ██╔╝
        ██║      ██║ ██████╔╝ ██║ █████╔╝ 
        ██║      ██║ ██╔══██╗ ██║ ██╔═██╗ 
        ███████╗ ██║ ██║  ██║ ██║ ██║  ██╗
        ╚══════╝ ╚═╝ ╚═╝  ╚═╝ ╚═╝ ╚═╝  ╚═╝
        """)
    print("~"*50)   
    pemain = input("Masukkan username     : ")
    mode = input("Mode (normal/expert)  : ")
    score = 0
    nyawa = 0 
    lagu = os.listdir("Lagu") #melihat daftar-daftar list yang ada pada direktori lagu

    while pemain == "null" or pemain == "": #pemain akan diminta terus sampai memberikan nama yang valid 
        pemain = input("Harap gunakan nama yang valid!\nMasukkan username     : ")
    while mode.lower() not in ["normal", "expert"]: #pemain akan diminta terus sampai memberikan mode yang valid
        mode = input("Mode (normal/expert)  : ")    

    if mode.lower() == "normal":
        nyawa = 3
    else:
        nyawa = 1

    print("~"*50)
    print("Good Luck & Have Fun :)\n")
    return pemain, mode, score, nyawa, lagu #kembaliin value pemain,mode,score,nyawa,dan lagu ke fungsi pemanggil
    
def generate_lagu(nama_file): 
    with open(f"lagu/{nama_file}", 'r') as lagu_terpilih:
        print("Judul lagu :",nama_file[:-4]) #dikasih batas sampai -4 supaya .txt nya tidak kebaca
        list_soal = [] #nampung 4 baris sebelumnya dalam sebuah lagu
        list_jawaban = [] #lirik jawaban
        lirik = lagu_terpilih.readlines()
        index_jawaban = random.randrange(4,len(lirik)-1) #pilih baris secara acak mulai dari index ke-4 sampai akhir yang nantinya sebagai lirik jawaban
        list_jawaban.append(lirik[index_jawaban])
        
        for k in range(index_jawaban-4,index_jawaban): #tampilin 4 baris sebelumnya yang nantinya sebagai lirik soal 
            list_soal.append(lirik[k])
        string_lirik_soal = "".join(list_soal)
        string_lirik_jawaban = "".join(list_jawaban)
    return string_lirik_soal, string_lirik_jawaban #return value lirik soal dan lirik jawaban

def ronde(pemain, mode, score, nyawa, lagu):       
    i = 1
    while i <= 6: 
        if nyawa > 0:
            print("Round",i)
            print(f"Nyawa : {nyawa}")
            print(f"Score : {score}")
            print("~"*50)
            nama_file = random.choice(lagu) #ambil sebuah lagu secara acak dari list folder lagu dan disimpen di sebuah variable buat manggil fungsi generate_lagu
            lirik_soal, lirik_jawaban = generate_lagu(nama_file)
            print(lirik_soal)
            tebakan = input("Silahkan menebak: \n")
            if i < 5: # ini untuk ronde 1-4 jika nyawa > 0
                if tebakan.lower() == lirik_jawaban[:-1].lower(): #lebih baik pakai method .lower biar lebih aman dalam mempertimbangkan jawaban user
                    score += len(lirik_jawaban) - 1 #di -1 karena di akhir baris setiap lirik mengandung new line
                    print("Jawaban BENAR\n")
                elif tebakan.lower() != lirik_jawaban[:-1].lower():
                    nyawa -= 1
                    print("Jawaban Anda SALAH")
                    print(f"Jawaban yang benar adalah {lirik_jawaban[:-1]}\n")

            if i == 5:   #untuk ronde 5 jika nyawa > 0
                if tebakan.lower() == lirik_jawaban[:-1].lower(): 
                    score += len(lirik_jawaban) - 1 
                    print("Jawaban BENAR\n")
                    print("SELAMAT!")    
                    print("Anda berhasil menyelesaikan permainan")
                    print("Hasil akhir :")
                    print(f"Score Anda adalah {score}")
                    break
                elif tebakan.lower() != lirik_jawaban[:-1].lower():
                    nyawa -= 1
                    print("Jawaban Anda SALAH")
                    print(f"Jawaban yang benar adalah {lirik_jawaban[:-1]}\n") 
                    if nyawa > 0: #saat di ronde 5 dan setelah jawaban tebakan user salah, tapi nyawanya masih > 0, maka perintah di bawah ini akan dijalankan
                        print("SELAMAT!")  #jika di ronde 5 nyawa user setelah nebak adalah 0 maka dia aka looping sekali dan mencetak elif nyawa == 0
                        print("Anda berhasil menyelesaikan permainan")
                        print("Hasil akhir :")
                        print(f"Score Anda adalah {score}")
                        break

        elif nyawa == 0:
            print("GAME OVER")
            print(f"Sayang sekali {pemain}, Anda terhenti di sini")
            print("Hasil akhir :")
            print(f"Score : {score}")
            break            
        i += 1
    return mode, score

pemain, mode, score, nyawa, lagu = start_game() #membuat 5 variable karena fungsinya mereturn 5 value
mode, score = ronde(pemain, mode, score, nyawa, lagu) #nampung hasil return di fungsi ronde

nilai_terbagus = os.listdir()
highscore = None
if "highscore.txt" not in nilai_terbagus: #jika file highscore belum ada maka akan dibuat terlebih dahulu
    with open("highscore.txt", "w") as my_file:
        isian = ["Normal null 0\n", "Expert null 0\n"]
        my_file.writelines(isian) #mengisi highscore.txt dengan tulisan yang ada di variable isian
baris_normal, nilai_normal = "", "" # buat nampung isi baris normal dan nilainya
baris_expert, nilai_expert = "", "" # buat nampung isi baris expert dan nilainya    
with open("highscore.txt", "r") as my_file:
    baca_baris = my_file.readlines()
    baris_normal += baca_baris[0] # misahin baris normal dan baris expert
    baris_expert += baca_baris[1]
for i in baris_normal:
    if i.isnumeric(): #memisahkan nilai dan karakter dari baris_normal
        nilai_normal += i
for i in baris_expert:
    if i.isnumeric(): #memisahkan nilai dan karakter dari baris_expert
        nilai_expert += i

def new_high_score(pemain,mode,score): #buat fungsi ini supaya kodingan tidak terlalu makan banyak baris
    ucapan1 = "NEW HIGH SCORE!!!"
    nama1 = f"pemain : {pemain}"
    nilai1 = f"score : {score}"
    sukses1 = f"Berhasil meraih highscore di mode {mode}"
    penutup1 = "~~~~~~~~~~~~~~~~ Thanks for playing ~~~~~~~~~~~~~~~~ "
    return ucapan1, nama1, nilai1, sukses1, penutup1
             
if mode == "normal" and score > int(nilai_normal): #jika nilai dari mode normal melebihi sebelumnya maka nilai tersebut akan dimasukkan ke highscore.txt 
    with open("highscore.txt", "w") as my_file:
        print(f"Normal {pemain} {score}", file = my_file) #nilai normal pada baris_normal akan diubah
        print(baris_expert, file = my_file) #nilai expert tetap
    ucapan,nama,nilai,sukses,penutup = new_high_score(pemain,mode,score)
    print("\n"+ucapan+"\n"+nilai+"\n"+sukses+"\n"+penutup)

elif mode == "expert" and score > int(nilai_expert):  #jika nilai dari mode expert melebihi sebelumnya maka nilai tersebut akan dimasukkan ke highscore.txt q
    with open("highscore.txt", "w") as my_file:
        print(baris_normal, file = my_file,end="") #nilai normal tetap
        print(f"Expert {pemain} {score}", file = my_file) #nilai expert pada baris_normal akan diubah
    ucapan,nama,nilai,sukses,penutup = new_high_score(pemain,mode,score)    
    print("\n"+ucapan+"\n"+nilai+"\n"+sukses+"\n"+penutup)   