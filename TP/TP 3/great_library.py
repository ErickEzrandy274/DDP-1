set_nama_rak = set() #nampung nama rak
set_nama_buku = set() #nampung nama buku
my_dict = {} #posisi rak terbaru
list_spesifikasi_buku = [] #berisikan nama buku, pengarang, tahun, penerbit, dan genre buku

def nama_buku(nama):
    if nama[3] not in set_nama_buku:
        if nama[2] not in set_nama_rak:
            set_nama_rak.add(nama[2])  
            print(f"Rak dengan nama {nama[2]} berhasil ditambahkan")
        set_nama_buku.add(nama[3])
        print(f"Buku dengan nama {nama[3]}, {nama[5]}, {nama[6]}, {nama[7]} berhasil ditambahkan\n")
        my_dict[nama[3]] = nama[2] #mengupdate posisi terbaru dari nama buku 
        list_spesifikasi_buku.append((nama[3:])) 

    else:
        print(f"Buku dengan nama {nama[3]} sudah ada di dalam sistem\n")

def nama_rak(name):
    if name[2] not in set_nama_rak:
        if perintah == "MOVE BUKU":
            if name[3] not in set_nama_buku:
                print(f"Buku dengan nama {name[3]} tidak ditemukan\n")

            else: 
                set_nama_rak.add(name[2])
                print(f"Rak dengan nama {name[2]} berhasil ditambahkan")   
                print(f"Buku dengan nama {name[3]} dipindahkan dari rak dengan nama {my_dict[name[3]]} ke rak dengan nama {name[2]}\n")
                my_dict[name[3]] = name[2]        

        else: #perintah "ADD RAK"
            set_nama_rak.add(name[2])
            print(f"Rak dengan nama {name[2]} berhasil ditambahkan\n")
    
    else:
        if perintah == "MOVE BUKU":
            if name[3] not in set_nama_buku:
                print(f"Buku dengan nama {name[3]} tidak ditemukan\n")

            else:   
                print(f"Buku dengan nama {name[3]} dipindahkan dari rak dengan nama {my_dict[name[3]]} ke rak dengan nama {name[2]}\n")
                my_dict[name[3]] = name[2]
        else:
            print(f"Rak dengan {name[2]} sudah ada di dalam sistem\n")

def spec_buku1(namebook,indekss=0):
    if indekss == len(set_nama_buku): #Base case 
        print(f"Buku dengan nama {namebook} tidak ditemukan\n")
    
    else: #Recursion case
        if list_spesifikasi_buku[indekss][0] == namebook:
            print("Buku ditemukan")
            print(f"Posisi: {my_dict[namebook]}")
            print(f"Nama Buku: {namebook}")
            print(f"Pengarang: {list_spesifikasi_buku[indekss][1]}")
            print(f"Penerbit Buku: {list_spesifikasi_buku[indekss][2]}")
            print(f"Tahun Terbit: {list_spesifikasi_buku[indekss][3]}")
            print(f"Genre Buku: {list_spesifikasi_buku[indekss][4]}\n")

        else:
            spec_buku1(namebook, indekss+1)

while True:
    print("Selamat datang di The The Great Library")
    print("Silahkan masukkan perintah!")
    inputan = input("Perintah anda: ").split()
    if len(inputan) == 2: #program ini akan jalan jika objek yang mau ditambahkan berupa string kosong 
        print("Perintah tidak valid\n")
        continue
    perintah = " ".join(inputan[:2]) #untuk mengetahui apa perintah dari user
    if perintah == "ADD RAK":
        nilai = True
        while nilai:
            if inputan[2] == "null": #menghandle jika user mengisi nama rak dengan "null"
                inputan = input("Perintah anda: ").split()
            else:
                nilai = False
        nama_rak(inputan)
        
    elif perintah == "ADD BUKU":
        nilai = True
        while nilai:
            if inputan[3] == "null": #menghandle jika user mengisi nama buku dengan "null"
                inputan = input("Perintah anda: ").split()
            else:
                nilai = False  
        inputan[5],inputan[6] = inputan[6],inputan[5] #supaya nama rak tetap di index ke-2
        nama_buku(inputan)
        
    
    elif perintah == "MOVE BUKU":
        nilai = True
        while nilai:
            if inputan[2] == "null": #menghandle jika user mengisi nama buku dengan "null"
                inputan = input("Perintah anda: ").split()
            else:
                nilai = False 
        inputan[2],inputan[3]= inputan[3],inputan[2] #supaya nama rak tetap di index ke-2
        nama_rak(inputan)

    elif perintah == "SEARCH BUKU":
        spec_buku1(inputan[2])
            
    elif perintah == "EXIT":
        print("Selesai, Sistem dimatikan")
        break
    else:
        print("Perintah tidak dikenali\n")