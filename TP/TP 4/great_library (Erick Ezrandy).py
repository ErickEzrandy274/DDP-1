#Berkolaborasi dengan Indra
class Book:
    def __init__(self,nama,tahun,pengarang,penerbit):
        self.__nama = nama
        self.__tahun = tahun
        self.__pengarang = pengarang
        self.__penerbit = penerbit

    def get_nama_buku(self):
        return self.__nama 

    def get_tahun(self):
        return self.__tahun
    
    def get_pengarang(self):
        return self.__pengarang
    
    def get_penerbit(self):
        return self.__penerbit 
    
    def get_book_description(self):
        print(f"Nama Buku: {self.get_nama_buku()}\nTahun: {self.get_tahun()}\
        \nPengarang: {self.get_pengarang()}\nPenerbit: {self.get_penerbit()}")

    def __eq__(self,nama_buku):
        return self.get_nama_buku() == nama_buku

    def __lt__(self,other):
        return self.get_nama_buku() < other.get_nama_buku()
    
class FictionBook(Book):
    def __init__(self,nama,tahun,pengarang,penerbit,genre):
        super().__init__(nama,tahun,pengarang,penerbit)
        self.__genre = genre
    
    def get_genre(self):
        return self.__genre
    
    def get_book_description(self):
        print("Buku Fiksi")
        super().get_book_description()
        print(f"Genre : {self.get_genre()}\n")

class ReferenceBook(Book):
    def __init__(self,nama,tahun,pengarang,penerbit,kota_terbit):
        super().__init__(nama,tahun,pengarang,penerbit)
        self.__kota_terbit = kota_terbit
    
    def get_kota_terbit(self):
        return self.__kota_terbit
    
    def get_book_description(self):
        print("Buku Referensi")
        super().get_book_description()
        print(f"Kota Terbit : {self.get_kota_terbit()}\n")

class EncyclopediaBook(Book):
    def __init__(self,nama,tahun,pengarang,penerbit,genre,revisi_num):
        super().__init__(nama,tahun,pengarang,penerbit)
        self.__revisi_num = revisi_num
    
    def get_revisi_num(self):
        return self.__revisi_num

    def get_book_description(self):
        print("Buku Ensiklopedia")
        super().get_book_description()
        print(f"Nomor Revisi : {self.get_revisi_num()}\n")

class Shelf:
    def __init__(self,nama_rak,kumpulan_buku):
        self.__nama_rak = nama_rak
        self.__kumpulan_buku = kumpulan_buku 

    def get_nama_rak(self):
        return self.__nama_rak

    def get_kumpulan_buku(self):
        return self.__kumpulan_buku
    
    def set_kumpulan_buku(self,nama_buku):
        self.get_kumpulan_buku().append(nama_buku)
    
    def search_buku(self,nama_buku):
        self.get_book_description(nama_buku) #self di sini merujuk ke objek rak
    
    def list_buku(self):
        self.get_kumpulan_buku().sort() #diurutkan
        for j in self.get_kumpulan_buku():
            print(f"{'-':^5s}{j.get_nama_buku()}")

class KnowledgeShelf(Shelf):
    def __init__(self,nama,kumpulan_buku):
        super().__init__(nama,kumpulan_buku)
 
    def add_buku(self,nama_buku):
        self.set_kumpulan_buku(nama_buku)

class WorldShelf(Shelf):
    def __init__(self,nama,kumpulan_buku):
        super().__init__(nama,kumpulan_buku) 

    def add_buku(self,nama_buku):
        self.set_kumpulan_buku(nama_buku)

class MysteryShelf(Shelf):

    def __init__(self,nama,kumpulan_buku):
        super().__init__(nama,kumpulan_buku) 

    def add_buku(self,nama_buku):
        self.set_kumpulan_buku(nama_buku)

class CompendiumShelf(Shelf):

    def __init__(self,nama,kumpulan_buku):
        super().__init__(nama,kumpulan_buku) 

    def add_buku(self,nama_buku):
        self.set_kumpulan_buku(nama_buku)

class Library:
    def __init__(self,kumpulan_rak):
        self.__kumpulan_rak = kumpulan_rak
    
    def get_kumpulan_rak(self):
        return self.__kumpulan_rak
    
    def set_kumpulan_rak(self, rak_lain,):
        self.__kumpulan_rak.append(rak_lain)
    
    def add_rak(self, rak_lain):
        self.set_kumpulan_rak(rak_lain)

    def add_buku(self,name_buku,name_rak):
        name_rak.add_buku(name_buku) #name_rak ini dipakai untuk meletakkan buku di rak mana 
    
    def search_buku(self,nama_buku,name_rak):
        name_rak.search_buku(nama_buku) 

def get_objek_rak(nama_rak,list_rak):
    for objek in list_rak: #fungsi ini berguna untuk mendapatkan objek rak
        if objek.get_nama_rak() == nama_rak:
            return objek

def get_objek_buku(nama_buku,list_buku):
    for objek in list_buku:  #fungsi ini berguna untuk mendapatkan objek buku          
        if objek.get_nama_buku() == nama_buku:
            return objek

def nama_rak(list_rak,nama_rak):
    for i in list_rak: #cek apakah sudah ada atau belum nama raknya
        if i.get_nama_rak() == nama_rak: #jika nama rak sudah ada maka akan mereturn False
            return False
    return True

def nama_buku(list_buku,nama_buku):
    for i in list_buku:  #cek apakah sudah ada atau belum nama bukunya
        if i == nama_buku: #masuk ke def __eq__
            return i == nama_buku #kalau True maka bukunya sudah ada
    return False

def main():
    library = Library([]) 
    list_rak = [] #nampung objek rak
    list_buku = [] #nampung objek buku

    #di dalam class Library ada rak ini secara default
    rak_pengetahuan, rak_dunia = KnowledgeShelf("Pengetahuan01",[]), WorldShelf("Dunia01",[])
    rak_misteri, rak_compendium = MysteryShelf("Misteri01",[]), CompendiumShelf("Compendium01",[])

    library.add_rak(rak_pengetahuan), library.add_rak(rak_dunia)
    library.add_rak(rak_misteri), library.add_rak(rak_compendium)
    list_rak.extend([rak_pengetahuan,rak_dunia,rak_misteri,rak_compendium])

    while True:
        print("Selamat datang di The The Great Library")
        print("Silahkan masukkan perintah!")
        inputan = input("Perintah Anda: ").split()
        if (" ".join(inputan[:2])) == "ADD RAK":
            if len(inputan) != 4: 
            #kalau panjang inputan dari user tidak sama dengan 4, handling ini akan di-run
                print("Perintah tidak valid!\n")
                continue

            if inputan[3] == 'Dunia' or inputan[3] == 'Pengetahuan' \
                or inputan[3] == 'Misteri' or inputan[3] == 'Compendium': 
                if nama_rak(list_rak,inputan[2]):
                    if inputan[3] == 'Dunia':
                        dunia = WorldShelf(inputan[2],[])
                        library.add_rak(dunia), list_rak.append(dunia)
                        print(f"Rak baru berhasil ditambahkan\n\nNama: {inputan[2]}\nJenis: {inputan[3]}\n")

                    elif inputan[3] == 'Pengetahuan':
                        pengetahuan = KnowledgeShelf(inputan[2],[])
                        library.add_rak(pengetahuan), list_rak.append(pengetahuan)
                        print(f"Rak baru berhasil ditambahkan\n\nNama: {inputan[2]}\nJenis: {inputan[3]}\n")

                    elif inputan[3] == 'Misteri':
                        misteri = MysteryShelf(inputan[2],[])
                        library.add_rak(misteri), list_rak.append(misteri) 
                        print(f"Rak baru berhasil ditambahkan\n\nNama: {inputan[2]}\nJenis: {inputan[3]}\n")

                    elif inputan[3] == 'Compendium':
                        compendium = CompendiumShelf(inputan[2],[])
                        library.add_rak(compendium), list_rak.append(compendium) 
                        print(f"Rak baru berhasil ditambahkan\n\nNama: {inputan[2]}\nJenis: {inputan[3]}\n")

                else:
                    print(f"Rak dengan nama {inputan[2]} sudah ada di dalam sistem\n")
            else:
                print(f"Tidak dapat menambahkan Rak dengan jenis {inputan[3]}\n")

        elif (" ".join(inputan[:2])) == "ADD BUKU":
            if len(inputan) != 9: 
            #kalau panjang inputan dari user tidak sama dengan 9, handling ini akan di-run
                print("Perintah tidak valid!\n")
                continue
            
            if nama_rak(list_rak,inputan[2]) == False: #nama rak sudah ada
                if nama_buku(list_buku,inputan[3]) == False: #nama buku belum ada
                    if inputan[7] == "Fiksi":
                        if type(get_objek_rak(inputan[2],list_rak)) != KnowledgeShelf:
                            objek_rak = get_objek_rak(inputan[2],list_rak) #mendapatkan objek rak
                            buku = FictionBook(inputan[3],inputan[4],inputan[5],inputan[6],inputan[8]) #membuat objek buku dari kelas Fiksi
                            library.add_buku(buku,objek_rak), list_buku.append(buku)
                            print(f"Buku baru berhasil ditambahkan pada rak {inputan[2]}\n")
                            buku.get_book_description() #mencetak deskripsi objek buku
                        else:
                            print("Buku gagal ditambahkan :(\n")
                            
                    elif inputan[7] == "Referensi":
                        if type(get_objek_rak(inputan[2],list_rak)) != WorldShelf:
                            objek_rak = get_objek_rak(inputan[2],list_rak)
                            buku = ReferenceBook(inputan[3],inputan[4],inputan[5],inputan[6],inputan[8]) #membuat objek buku dari kelas Referensi
                            library.add_buku(buku,objek_rak), list_buku.append(buku)
                            print(f"Buku baru berhasil ditambahkan pada rak {inputan[2]}\n")
                            buku.get_book_description()
                        else:
                            print("Buku gagal ditambahkan :(\n")

                    elif inputan[7] == "Ensiklopedia":
                        if type(get_objek_rak(inputan[2],list_rak)) != MysteryShelf:
                            objek_rak = get_objek_rak(inputan[2],list_rak)
                            buku = EncyclopediaBook(inputan[3],inputan[4],inputan[5],inputan[6],inputan[8]) #membuat objek buku dari kelas Ensiklopedia
                            library.add_buku(buku,objek_rak), list_buku.append(buku)
                            print(f"Buku baru berhasil ditambahkan pada rak {inputan[2]}\n")
                            buku.get_book_description()
                        else:
                            print("Buku gagal ditambahkan :(\n")
                    else:
                        print("Buku gagal ditambahkan :(\n") 
                else: #jika buku sudah ada akan di-run program ini
                    print(f"Buku dengan nama {inputan[3]} sudah ada di dalam sistem\n")  

            else: #nama rak belum ada
                print("Buku gagal ditambahkan :(\n")           

        elif (" ".join(inputan[:1])) == "SEARCH":
            #perintah 'SEARCH' akan mencetak deskripsi buku jika objek bukunya ada di list_buku 
            if get_objek_buku(inputan[1],list_buku) in list_buku:
                buku = get_objek_buku(inputan[1],list_buku) 
                print("Buku ditemukan\n")
                for i in library.get_kumpulan_rak():
                    if buku in i.get_kumpulan_buku(): #kalo nama bukunya ada di rak tersebut maka 
                        library.search_buku(buku,i)   #akan mencetak deskripsi buku
            else:
                print(f"Buku dengan nama {inputan[1]} tidak ditemukan\n")
        
        elif (" ".join(inputan[:1])) == "LIST":
            if len(list_buku) > 0:
                for i in list_rak:
                    print(f"\n{i.get_nama_rak()}") #mencetak nama rak
                    i.list_buku() #mencetak nama buku yang ada di rak-rak tersebut
                print()

            else:
                print("Belum ada buku di dalam sistem :(\n")    
                                       
        elif (" ".join(inputan[:1])) == "EXIT":
            print("Selesai, Sistem dimatikan")
            break

        else: #jika user langsung klik 'ENTER' maka akan masuk ke sini
            print("Perintah tidak dikenali\n") 
           
if __name__ == "__main__":
    main()