from tkinter import *
from PIL import Image, ImageTk
from tkinter.font import *

class AddBuku(Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.master = master
        self.initForm()
        self.create_widget()

    def initForm(self):
        ico = Image.open('form.jpg')
        photo = ImageTk.PhotoImage(ico)
        self.master.wm_iconphoto(False, photo)
        self.master.title("ADD BUKU")
        self.master.geometry("250x260")

    def create_widget(self):
        font1 = Font(family='Times New Roman', size=10, weight='bold' )
        font2 = Font(family='Times New Roman', size=12, weight='bold' )
        label1 = Label(self.master, text="Form Tambah Buku",font=font2, fg='red')
        label1.grid(column=1)

        label2 = Label(self.master, text="Jenis",font=font1)
        label2.grid(row=1,column=0)
        self.nama = StringVar()
        entry2 = Entry(self.master, textvariable=self.nama, width=20)
        entry2.grid(row=1,column=1,columnspan=2,pady=4,padx=5)

        label3 = Label(self.master, text="Rak",font=font1)
        label3.grid(row=2,column=0)
        self.nama = StringVar()
        entry3 = Entry(self.master,textvariable=self.nama, width=20)
        entry3.grid(row=2,column=1,columnspan=2,pady=4,padx=5)

        label4 = Label(self.master, text="Nama Buku",font=font1)
        label4.grid(row=3,column=0)
        self.nama = StringVar()
        entry4 = Entry(self.master, textvariable=self.nama, width=20)
        entry4.grid(row=3,column=1,columnspan=2,pady=4,padx=5)

        label5 = Label(self.master, text="Tahun",font=font1)
        label5.grid(row=4,column=0)
        self.nama = StringVar()
        entry5 = Entry(self.master, textvariable=self.nama, width=20)
        entry5.grid(row=4,column=1,columnspan=2,pady=4,padx=5)

        label6 = Label(self.master, text="Penulis",font=font1)
        label6.grid(row=5,column=0)
        self.nama = StringVar()
        entry6 = Entry(self.master, textvariable=self.nama, width=20)
        entry6.grid(row=5,column=1,columnspan=2,pady=4,padx=5)

        label7 = Label(self.master, text="Penerbit",font=font1)
        label7.grid(row=6,column=0)
        self.nama = StringVar()
        entry7 = Entry(self.master, textvariable=self.nama, width=20)
        entry7.grid(row=6,column=1,columnspan=2,pady=4,padx=5)

        label8 = Label(self.master, text="Extra",font=font1)
        label8.grid(row=7,column=0)
        self.nama = StringVar()
        entry8 = Entry(self.master, textvariable=self.nama, width=20)
        entry8.grid(row=7,column=1,columnspan=2,pady=4,padx=5)

        button1 = Button(self.master, text="Submit", command="ADD BUKU",activeforeground='white',\
                    activebackground='red',background='light blue',font=font2)
        button1.grid(column=1,pady=4)

class AddRak(Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.master = master
        self.initForm()
        self.create_widget()
    
    def initForm(self):
        ico = Image.open('form.jpg')
        photo = ImageTk.PhotoImage(ico)
        self.master.wm_iconphoto(False, photo)
        self.master.title("ADD RAK")
        self.master.geometry("230x125")
    
    def create_widget(self):
        font1 = Font(family='Times New Roman', size=11, weight='bold' )
        font2 = Font(family='Times New Roman', size=12, weight='bold' )
        label1 = Label(self.master, text="Form Tambah Rak",font=font2, fg='blue')
        label1.grid(column=1, columnspan=4)

        label2 = Label(self.master, text="Nama Rak",font=font1)
        label2.grid(row=1,column=0)
        self.nama1 = StringVar()
        entry2 = Entry(self.master, textvariable=self.nama1, width=20)
        entry2.grid(row=1,column=1,columnspan=2,pady=4,padx=5)

        label3 = Label(self.master, text="Jenis Rak",font=font1)
        label3.grid(row=2,column=0)
        self.nama2 = StringVar()
        entry3 = Entry(self.master,textvariable=self.nama2, width=20)
        entry3.grid(row=2,column=1,columnspan=2,pady=4,padx=5)

        button1 = Button(self.master, text="Submit",activeforeground='white',\
                    activebackground='red',background='light blue',font=font2)
        button1.grid(column=1,pady=4)

    

class SearchBuku(Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.master = master
        self.initForm()
        self.create_widget()

    def initForm(self):
        ico = Image.open('form.jpg')
        photo = ImageTk.PhotoImage(ico)
        self.master.wm_iconphoto(False, photo)
        self.master.title("SEARCH BUKU")
        self.master.geometry("308x100")   

    def create_widget(self):
        font1 = Font(family='Times New Roman', size=12, weight='bold' )
        font2 = Font(family='Times New Roman', size=13, weight='bold' )
        label1 = Label(self.master, text="Masukkan Nama Buku yang Anda ingin cari!",font=font1, fg='red')
        label1.grid(row=1)
        self.nama = StringVar()
        entry1 = Entry(self.master, textvariable=self.nama, width=20)
        entry1.grid(row=2,columnspan=2,pady=4,padx=5)

        button1 = Button(self.master, text="Submit", command="ADD BUKU",activeforeground='white',\
                    activebackground='red',background='light blue',font=font2)
        button1.grid(row=3,pady=4)

if __name__ == "__main__":
    window = Tk()
    app = AddRak(master=window)
    app.mainloop()
