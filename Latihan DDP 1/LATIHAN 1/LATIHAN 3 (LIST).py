a = [-3,-1,1,3]               #-3 itu indexnya 0, -1 itu indexnya 1
a.append(0)
a.append("Hello World!")      #a.append = untuk menambahkan string/integer
a.append([-5,2,5])
a.pop()                       #a.pop = untuk menghapus string/integer

b = ["Semarang", "Bandung", "Yogyakarta"]
temp = b[0]                   #Kalau mau swap seperti ini juga bisa :
b[0] = b[2]                   #b[0] = b[2] 
b[2] = temp                   #b[2] = b[0]

c = [-3,-2,0]
c.append(2)
c.append(4)
c.pop(2)
c[0]= 1000
c[2]= 500

d = [9,6,3,-3]
d.append(4)
d.pop(1)                      #d.pop(1) artinya apus list di index 1
d[2]= 34567

