# a collection of instructions
# a collection of code

def function1():         
     print("Welcome to UI")        # ini tidak keprint karena fungsi def hanya sebagai collection         
     print("Maba UI 2020")         # kalau mau print ini, tulis function1() saat run the program
     
print("Maba Fasilkom UI 2020")

# a mapping
# input or an argument
def function2(x):
     return 2*x
A = function2(5)                   # return value or output

def function3(x, y):              # ini untuk pengoperasian
     return x+y
B = function3(4, 6)

def function3(x,y):
     return x/y
C = function3(4,6)

def function4(x):
     print(x)
     print("Still in this function")
     return x*5
D = function4(4)

def function5(some_argument):
     print(some_argument)
     print("Perfecto!")

E = function5(3)

#BMI_Calculator
name1 = "Erick Ezrandy"
height_m1 = 1.66
weight_kg1 = 64

name2 = "Alfred Alexander"
height_m2 = 1.75
weight_kg2 = 75
def BMI_Calculator(name, height_m, weight_kg):
     BMI = weight_kg / (height_m**2)
     print("BMI = ")
     print(BMI)
     if BMI < 25:
          return name + " is not overweight"
     else:
          return name + " is overweight"
result1 = BMI_Calculator(name1, height_m1, weight_kg1)        #untuk jalankan ini tulis print(result1)  
result2 = BMI_Calculator(name2, height_m2, weight_kg2)



name3 = "Benzz Knight"
height_m3 = 1.78
weight_kg3 = 85

name4 = "Gregory"
height_m4 = 1.85
weight_kg4 = 78

name5 = "Anonymous"
height_m5 = 1.9
weight_kg5 = 88

def BMI_Calculator(name,height_m,weight_kg):
     BMI = weight_kg / (height_m**2)
     print("BMI =  ")
     print(BMI)
     if BMI < 25:
          return name + " is not overweight"
     else:
          return name + " is overweight"

result3 = BMI_Calculator(name3,height_m3, weight_kg3)
result4 = BMI_Calculator(name4,height_m4, weight_kg4)
result5 = BMI_Calculator(name5,height_m5, weight_kg5)

