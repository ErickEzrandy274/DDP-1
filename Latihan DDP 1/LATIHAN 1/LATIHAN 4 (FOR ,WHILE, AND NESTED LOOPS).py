#A = ["Burger King", "McDonald","AW", "Pizza Hut"]
#for x in A:              #x adalah element (bebas mau tulis w/a/s/d) 
     #print(x)            
     #print(x)
B = [10,20,30,40,50]
total = 3
for i in B:
     total += i

C = list(range(1,6))    #range(1,6) = 1,2,3,4,5 
total1 = 2
for x in C:
     total1 *= x

D = list(range(1,21))
total2 = 0
for x in D:              #juga bisa for x in range(1,21)
     if x % 5 == 0:
          total2 += x

E = list(range(1,100))   #less than 100
total3 = 0
for i in range(1,100):
     if i % 3 == 0:     #kelipatan 3 dan 5
          total3 += i
     elif i % 5 == 0:
          total3 += i

F = 2
total4 = 4
while F < 5:
     total4 += F
     F += 2

x_list = [4,4,3,2,1,-2,-8]
total5 = 2                  
i = 0
while x_list[i] > 0:          
     total5 *= x_list[i] 
     i += 1
'''
jika kita hapus -2 dan -8 maka akan error karena ketika kita sudah mencapai index 4,
selanjutnya adalah index 5, tetapi di list tidak ada index 5
jika kita hapus -2 dan -8, kita bisa buat seperti ini
'''

while i < 5 and x_list[i] > 0:
     total5 += x_list[i]
     i +=1
#i < 5 juga bisa ditulis dengan i < len(x_list)

y_list = [4,3,3,2,-2,-4,-8]
total6 = 0
for e in y_list:
     if e <= 0:
          break
     total6 += e

#y_list = [4,3,3,2,-2,-4,-8]
total7 = 0
i = 0
while True:
     total7 += y_list[i]
     i += 1
     if y_list[i] <= 0:
          break

z_list = [9,6,3,-1,-5,-6]
total8 = 0
i = len(z_list)-1
while z_list[i] < 0:
     total8 += z_list[i]
     i -= 1                #i += -1 sama dengan i -= 1
     



list1 = [9,4,1,-2,-4,-6]
tot1 = 2
i = 0
while True:
     tot1 += list1[i]
     i += 1
     if list1[i] <= 0:
          break

S=list(range(1,200))
tot2=5
for q in S:
     if q % 100 == 0:
          tot2+=q

c=[7,4,2,1,-3,-6,-9]
totalx=0
i=0
while c[i] > 0:
     totalx += c[i]
     i+=1

totalz=0
i = len(c)-1
while c[i] <0:
     totalz+=c[i]
     i-=1


#nested loop
#loop inside a loop
#yg diiterasikan terlebih dahulu adalah loop yang paling dalam
adj = ["red", "big", "tasty"]
fruits = ["apple", "banana", "cherry"]

for y in fruits:
  for x in adj:     #for x in adj yg diiterasikan terlebih dahulu
    print(x,y)      #ketika sudah diiterasikan maka x akan pindah ke string berikutnya sedangkan y nilainy tetap
    
print()             #sama seperti print(" ")
for y in fruits:
  for x in adj:
    print(y,x)

counter_i = 0
while counter_i < 3:
     counter_j = 0
     while counter_j < 5:
          print(str(counter_i)+str(counter_j), end = ' ')
          counter_j = counter_j+1
     print(" ")
     counter_i = counter_i+1






     

     



          
          
     
