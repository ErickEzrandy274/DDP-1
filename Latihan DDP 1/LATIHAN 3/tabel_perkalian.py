print("{:3s}{:s}".format("*","|"),end="")
for i in range(1,13):
    for j in range(1,13):
        print("{:8d}".format(i*j), end=" ")
    print()
print("-"*81)

#cek apakah suatu input merupakan alnum, alpha,digit,lower, ataupun upper
s = input("Masukkan input: ")
lis = [False]*5

for i in range(len(s)):
    if lis[0] == False:
        if s[i].isalnum():
            lis[0] = True
    if lis[1] == False:
        if s[i].isalpha():
            lis[1] = True
    if lis[2] == False:        
        if s[i].isdigit():
            lis[2] = True
    if lis[3] == False:
        if s[i].islower():
            lis[3] = True
    if lis[4] == False:        
        if s[i].isupper():
            lis[4] = True
for k in lis:
    print(k)


thickness = int(input("masukkan input: ")) #This must be an odd number
c = 'H'

#Top Cone
for i in range(thickness):
    print((c*i).rjust(thickness-1)+c+(c*i).ljust(thickness-1))

#Top Pillars
for i in range(thickness+1):
    print((c*thickness).center(thickness*2) + (c*thickness).center(thickness*6))

#Middle Belt
for i in range((thickness+1)//2):
    print((c*thickness*5).center(thickness*6))    

#Bottom Pillars
for i in range(thickness+1):
    print((c*thickness).center(thickness*2)+(c*thickness).center(thickness*6))    

#Bottom Cone
for i in range(thickness):
    print(((c*(thickness-i-1)).rjust(thickness)+c+(c*(thickness-i-1)).ljust(thickness)).rjust(thickness*6))    
