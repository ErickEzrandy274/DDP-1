import textwrap

def wrap(string, max_width):
    panjang = len(string)
    string_kosong = ""
    while panjang > 0:
        string_kosong += string[:max_width] + "\n"
        string = string[max_width:]
        panjang -= max_width  
    return string_kosong
    
    
if __name__ == '__main__':
    string, max_width = input("Masukkan string: "), int(input("Masukkan max_width: "))
    result = wrap(string, max_width)
    print(result)