A = input("Masukkan input: ")#masukkan 2 elemen dengan elemen kedua adalah 3 kalinya elemen pertama
A1, A2 = A.split() #pisahin inputan karena ada 2 elemen di dalamnya
N, M = int(A1),int(A2)

O = "|.."
P = "..|"
j = 1   
for i in range(N):
    if (i == 0) or (i == (N-1)):
        print(".|.".center(M,'-'))

    elif i == (N // 2):
        print("WELCOME".center(M,'-')) 

    elif 0 < i < N // 2:
        print(("."+ O*i + "|" + P*i + ".").center(M,'-'))   

    elif N // 2 < i < N-1:
        # O dikali (N//2)-j karena setelah mencetak baris tengah, sisanya sama seperti baris sebelum baris tengah sampai baris awal!
        print(("."+ O*((N//2)-j) + "|" + P*((N//2)-j) + ".").center(M,'-'))
        j += 1
    
    