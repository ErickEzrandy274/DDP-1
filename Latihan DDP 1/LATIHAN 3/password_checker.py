print("Syarat membuat password: ")
print("1. Password harus memiliki minimal 6 karakter dan maksimal 20 karakter.")
print("2. Setiap karakter harus berupa alfanumerik (a-z, A-Z, dan 0-9).")
print("3. Harus mengandung sedikitnya sebuah huruf kecil, sebuah huruf besar, dan sebuah angka.")
y=input("Masukkan password Anda: ")
a=0
b=0
c=0
for i in y: 
    if y.islower():
        a+=1
    elif y.isupper():
        b+=1
    elif y.isdigit():
        c+=1
    else:
        a=0 
        break
if a>0 and b>0 and c>0:
    print("Password Anda valid")  
else:
    print("Password Anda tidak valid")