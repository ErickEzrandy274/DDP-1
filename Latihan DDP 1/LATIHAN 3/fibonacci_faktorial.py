def fibonacci(n):
   if n == 0 or n == 1:
      return 1
   else:
      return (fibonacci(n-1) + fibonacci(n-2))      

x = int(input("Masukan Batas Deret Bilangan Fibonacci : "))
print("Deret Fibonacci")
for i in range(x):
   print(fibonacci(i),end=' ')

print()   
def faktorial(a):
   if a <= 1:
      return 1
   else:
      return a * faktorial(a-1)

bil = int(input("Masukan Bilangan : "))

print("%d! = %d" % (bil, faktorial(bil)))

