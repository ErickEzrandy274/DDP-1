print("                     DATA NILAI MAHASISWA")
print("------------------------------------------------------------------")
nama=input("                Nama  : ")
tugas=int(input("                Tugas : "))
uts=int(input("                UTS   : "))
uas=int(input("                UAS   : "))
nilai_akhir=0.25*tugas+0.35*uts+0.4*uas
if nilai_akhir>=75:
    grade="A"
elif 60<=nilai_akhir<=75:
    grade="B"
elif 45<=nilai_akhir<=60:
    grade="C"
else:
    grade="D"
print("------------------------------------------------------------------")
print()
print("                     NILAI AKHIR DAN GRADE")
print("------------------------------------------------------------------")
print("                Nama        :",nama)
print("                Nilai Akhir :",nilai_akhir)
print("                Grade       :",grade)