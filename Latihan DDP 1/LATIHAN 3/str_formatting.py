number = int(input())
'''decimal = []
octal = []
hexadecimal = []
binary = []
kode = ['A','B','C','D','E','F']
indeks = 0

for i in range(1,inputan+1):
    decimal.append(i)

for j in range(1,inputan+1):
    if j < 8:
        octal.append(j)
    else: 
        hasil_octal = []
        while j != 0: 
            sisa_bagi = j % 8
            hasil_octal.append(str(sisa_bagi)) 
            j //= 8 
        C = "".join(hasil_octal)  
        hasil = C[::-1]
        octal.append(int(hasil))

for k in range(1,inputan+1):
    if k < 10:
        hexadecimal.append(k)
    elif 10 <= k <= 15:
        hexadecimal.append(kode[indeks])
        indeks += 1   
    else: 
        hasil_hexadecimal = []
        while k != 0: 
            sisa_bagi = k % 16
            hasil_hexadecimal.append(str(sisa_bagi)) 
            k //= 16 
        C = "".join(hasil_hexadecimal)  
        hasil = C[::-1]
        hexadecimal.append(int(hasil))

for l in range(1,inputan+1):
    hasil_biner = []
    while l != 0: 
        sisa_bagi = l % 2
        hasil_biner.append(str(sisa_bagi)) 
        l //= 2
    C = "".join(hasil_biner)  
    hasil = C[::-1]
    binary.append(int(hasil))'''

#cara cepet pake bawaan piton
if number < 4:
    for a in range(1,number + 1):
        print('{:>2d}{:>3s}{:>3s}{:>3s}'.format(a,oct(a)[2:],(hex(a)[2:]).capitalize(),bin(a)[2:]))
elif number < 8:      
    for a in range(1,number + 1):
        print('{:>3d}{:>4s}{:>4s}{:>4s}'.format(a,oct(a)[2:],(hex(a)[2:]).capitalize(),bin(a)[2:]))
elif number < 16:
    for a in range(1,number + 1):
        print('{:>4d}{:>5s}{:>5s}{:>5s}'.format(a,oct(a)[2:],(hex(a)[2:]).capitalize(),bin(a)[2:]))
elif number < 32:
    for a in range(1,number + 1):
        b = hex(a)[2:]
        c = (b[::-1]).capitalize()
        d = c[::-1]
        print('{:>5d}{:>6s}{:>6s}{:>6s}'.format(a,oct(a)[2:],d,bin(a)[2:])) 
elif number < 64:
    for a in range(1,number + 1):
        b = hex(a)[2:]
        c = (b[::-1]).capitalize()
        d = c[::-1]
        print('{:>6d}{:>7s}{:>7s}{:>7s}'.format(a,oct(a)[2:],d,bin(a)[2:]))
elif number < 128:
    for a in range(1,number + 1):
        b = hex(a)[2:]
        c = (b[::-1]).capitalize()
        d = c[::-1]
        print('{:>7d}{:>8s}{:>8s}{:>8s}'.format(a,oct(a)[2:],d,bin(a)[2:]))  

