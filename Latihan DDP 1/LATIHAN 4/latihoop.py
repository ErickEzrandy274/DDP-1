'''class MyClass:
    def __init__(self):
        self.__data1 = 'Public' #public data attribute
        self.__data2 = 'Private' #private data attribute
    
    def getData1(self):
        return self.__data1
    
    def getData2(self):
        return self.__data2
    
    def setData1(self, newData):
        self.__data1 = newData
    
    def setData2(self, newData):
        self.__data2 = newData
    
    def __str__(self):
        return f"Terdapat perubahan data"

obj = MyClass()
print(obj.getData1())
print(obj.getData2())
print("================CHANGE================")
obj.setData1("Protected")
obj.setData2("Default")
print(obj.getData1())
print(obj.getData2())
print(obj)

class MyClass:
    my_class_attr = "value of class attr"

    def my_method(self, param1):  
        print("Param1:", param1)  
        print("Object", str(self))  
        self.my_instance_attr = param1

my_instance1 = MyClass()  
my_instance2 = MyClass()

my_instance1.my_method("string of my_instance1")  
my_instance2.my_method("string of my_instance2")

print(my_instance1.my_instance_attr)
print(my_instance2.my_instance_attr)'''

'''class MyClass:

  def __init__(self):
    self.my_public_attr = "Public"
    self.__my_private_attr = "Private"

  def print_private(self):
    print(self.__my_private_attr)

obj = MyClass()
print(obj.my_public_attr)
obj.print_private()
#print(obj.__my_private_attr) #manggil dari luar sehingga error 
print(obj.__dict__)
obj.__my_private_attr = "Anaconda"
print(obj.__my_private_attr)

class MyClass:  pass

class MyChildClass(MyClass):  pass

print(MyChildClass.__bases__)  
print(MyClass.__bases__)  
print(object.__bases__)

my_class_instance = MyClass()  
my_childclass_instance = MyChildClass()

print(my_childclass_instance.__class__)  
print(type(my_childclass_instance))

fib_call_count = []
def fib(n):
    fib_call_count.append(n)
    if (n == 0 or n == 1):
        return n
    else:
        return fib(n-1) +fib(n-2)

ffib_call_count = []
def ffib(n, memo = {}):
    ffib_call_count.append(n)
    if (n == 0 or n == 1):
        return n
    try:
        return memo[n]
    except KeyError:
        result = ffib(n-1, memo) + ffib(n-2, memo)
        memo[n] = result
    return result
k = 6
print("fib({}): {}".format(k, fib(k)))
print(fib_call_count)
print("ffib({}): {}".format(k, ffib(k)))
print(ffib_call_count)

class A:
    a = 5
    def __init__(self):
        self.a = 10
class B(A):
    def __init__(self):
        self.a = super().a
class C(B):
    def myfun(self, b):
        return A.a * b
i1 = C()
i2 = A()
print(i1.a)
print(i2.a)
print(A.a)
print(B.a)
print(i1.myfun(2))
print(i2.myfun(2))

class MyWord:
    def __init__(self, word = ())
        self.word = word
    
wise_word = MyWord(('Aku', 'tak', 'akan', 'menyerah'))
for val in wise_word:
    print(val, end = ' ')'''

'''class Struk:
    def __init__(self, rincian):
        self.rincian = rincian
    def __str__(self):
        return str(self.rincian)
    # Overloading operator +
    def __add__(self, struk_lain):
        hasil = self.rincian
        for barang in struk_lain.rincian:
            if barang in hasil:
                hasil[barang] = self.rincian[barang] + struk_lain.rincian[barang]
        else:
            hasil[barang] = struk_lain.rincian[barang]
        return Struk(hasil)

struk_1 = Struk({'sabun': 2000, 'sikat gigi': 6000, 'deterjen': 2000,
'shampoo': 5000})
struk_2 = Struk({'sabun': 5000, 'sikat gigi': 12000, 'pewangi': 2500})
print('Struk 1 =', struk_1)
print('Struk 2 =', struk_2)
hasil = struk_1 + struk_2
print('Struk 1 + Struk 2 =', hasil)'''
