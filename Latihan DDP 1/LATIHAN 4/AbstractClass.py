from abc import ABCMeta, abstractmethod
import math

class Geometri():
    def __init__(self, color):
        self.__color = color

    @abstractmethod
    def getArea(self):
        pass

    @abstractmethod
    def getPerimeter(self):
        pass

class Lingkaran(Geometri):
    def __init__(self, color, radius):
        super().__init__(color)
        self.__radius = radius
    
    def getArea(self):
        return math.pi * (self.__radius**2)

    def getPerimeter(self):
        return math.pi * self.__radius * 2
    
    def getRadius(self):
        return self.__radius
    
    def setRadius(self, newRadius):
        self.__radius = newRadius

class Persegi(Geometri):
    def __init__(self, color, side):
        super().__init__(color)
        self.__side = side
    
    def getArea(self):
        return self.__side**2

    def getPerimeter(self):
        return self.__side * 4

    def getSide(self):
        return self.__side
    
    def setSide(self, newSide):
        self.__side = newSide

class PersegiPanjang(Geometri):
    def __init__(self, color, length, width):
        super().__init__(color)
        self.__length = length
        self.__width = width
    
    def getArea(self):
        return self.__length * self.__width

    def getPerimeter(self):
        return 2 * (self.__length + self.__width)

    def getLength(self):
        return self.__length
    
    def getWidth(self):
        return self.__width
    
    def setLength(self, newLength):
        self.__length = newLength

    def setWidth(self, newWidth):
        self.__length = newWidth

def main():
    rectangle = PersegiPanjang("Blue",10,5)
    square = Persegi("Black",10)
    circle = Lingkaran("Red",10)
    print(f"Persegi dengan sisi {square.getSide()} memiliki luas {square.getArea()} dan keliling {square.getPerimeter()}")
    print(f"Persegi panjang dengan panjang {rectangle.getLength()} dan lebar {rectangle.getWidth()} memiliki luas {rectangle.getArea()} dan keliling {rectangle.getPerimeter()}")
    print(f"Lingkaran dengan radius {circle.getRadius()} memiliki luas {circle.getArea():.2f} dan keliling {circle.getPerimeter():.2f}")

if __name__ == '__main__':
    main()