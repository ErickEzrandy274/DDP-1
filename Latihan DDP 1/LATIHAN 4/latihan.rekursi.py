#penggunaan rekursi harus dipastikan suatu saat masuk ke base case
'''def pangkat(x,y):
    if y < 1:
        return 1
    else:
        return x * pangkat(x,y-1) 

print(f"5 pangkat 3 = {pangkat(5,3)}")

def deret_aritmetika(n,a,b): #cetak deret aritmetika pakai rekursi
    if n == 0:
        return a
    else:
        return b + deret_aritmetika(n-1,a,b)    
        
x = int(input("Masukkan batas deret: "))
beda = int(input("Masukkan beda deret: "))
awal = int(input("Masukkan suku awal: "))
for i in range(x):
    print(deret_aritmetika(i,awal,beda), end=" ")
    
print()

def deret_geometri(n,a,r):
    if n == 0:
        return a #basisnya adalah suku awalnya
    else:
        return r * deret_geometri(n-1,a,r)    

batas = int(input("Masukkan batas deret: "))
rasio = int(input("Masukkan rasio deret: "))
awal = int(input("Masukkan suku awal: "))
for i in range(batas):
    print(deret_geometri(i,awal,rasio),end=" ")

print()
#hitung jumlah deret arit tanpa ditampung di suatu variable
def sum_deret_arit(n,a,b): 
    if n == 0:
        return 0
    else:
        return a + (n-1)*b + sum_deret_arit(n-1,a,b)    
        
x = int(input("Masukkan batas deret: "))
beda = int(input("Masukkan beda deret: "))
awal = int(input("Masukkan suku awal: "))
print(sum_deret_arit(x,awal,beda))

print()
#hitung jumlah deret geo tanpa ditampung di suatu variable
def sum_deret_geo(n,a,r): 
    if n == 0:
        return 0
    else:
        return a * (r**(n-1)) + sum_deret_geo(n-1,a,r)    
        
x = int(input("Masukkan batas deret: "))
rasio = int(input("Masukkan rasio deret: "))
awal = int(input("Masukkan suku awal: "))
print(sum_deret_geo(x,awal,rasio))

print()
#reverse suatu string
def misteri(kata):
    if len(kata) == 0:
        return ""
    else:
        return misteri(kata[1:]) + kata[0]
print(misteri("putih"))   

#mencari nilai biner dari suatu decimal
def biner(n):
    if n == 0 :
        return "" 
    else:
        return biner(n//2) + str(n % 2)
print(biner(13))         

def fibmemo(n, computed = {0:1, 1:1}):
    print("masuk fibmemo dengan n =",n)
    if n in computed:
        return computed[n]
    else:
        computed[n] = fibmemo(n-1,computed) + fibmemo(n-2,computed)
        return computed[n]  
print(fibmemo(5))  

#mencari jumlah kemunculan huruf pada kata
def freq(kata,huruf):
    if len(kata) == 0:
        return 0
    else:
        if kata[0] == huruf:
            return 1 + freq(kata[1:],huruf)
        else:
            return 0 + freq(kata[1:],huruf)    
print(freq("programming","m"))  
print(freq("mamialezatos","a"))          

#mencari tahu apakah suatu string dikatakan seimbang atau tidak
#seimbnag jika string kosong atau () atau (())
def balance(x):
    if len(x) <= 2:
        if "()" in x or len(x) == 0:
            return True
        else:
            return False
    else:
        if "()" in x:
            return balance(x.replace("()",""))
        else:
            return False  

#mengubah bilangan octal ke decimal
def decimal(x,power = 0):
    if x == 0 :
        return 0
    else:
        return (x%10) * 8 ** power + decimal(x//10,power+1)   
#print(decimal(10)) 

#cara lain, tetapi inputnya harus berupa string
def decimal1(x,power = 0):
    if x == '' :
        return 0
    else:
        return int(x[-1]) * 8 ** power + decimal1(x[:-1],power+1)  
#print(decimal1("100"))

#ngecek apakah suatu string merupakan palindrome atau tidak
def palindrome(string):
    if len(string) == 0 or len(string) == 1:
        return True
    else:
        if string[0] == "*" or string[1] == "*":
            return True and palindrome(string[1:-1])    
        else:
            return string[0] == string[-1] and palindrome(string[1:-1])    

def add(N,M):
    if M == 0:
        return N
    else:
        return add(N+1,M-1)
print(add(2,7)) '''

#mencari nilai max dari list 
def max_list(x):
    if len(x) == 1:
        return x[0]
    
    maxest = max_list(x[1:])
    if maxest > x[0]:
        return maxest
    else:
        return x[0]

print(max_list([2,3,4]))