'''bilangan = int(input("Masukkan angka: "))
n = 1
A = []

while bilangan > 0:
    pengurangan = bilangan - n
    A.append(pengurangan)
print(A)'''

newList = ['a' if x % 3 == 0 else 'b' if x % 3 == 1 else 'c' for x in range(10)]
print(newList)

newList1 = [1 if i % 2 == 0 else -1 for i in range(1,11)  ]
print(newList1)

def cetak_plus(N):
    if N % 2 != 0:
        for i in range(N):
            if i != N // 2:
                print(" "*(N//2)+"*"+" "*(N//2))
            else:    
                print("*"*N)
    else:
        print("Input harus berupa bilangan ganjil")            
cetak_plus(4)

def fun1(s):
    for i in "aiueo":
        s = s.replace(i,i.capitalize())
    print(s)

def fun2 (ls):
    for i in range (len(ls)):
        ls[i] += '*'
    print (ls)
s = "Peserta DDP 1 jujur dan kreatif"
ls = ["I", "love", "coding"]
